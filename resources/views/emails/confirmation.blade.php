<h1>Bonjour {{$user->name}}!</h1>
<p>Voici votre confirmation d'inscription pour l'activité "{{$event->name}}".</p>
<p>Veuillez vérifier les renseignements suivants :</p>
<ul>
    <li>Nom : {{$user->name}}</li>
    <li>Courriel : {{ $user->email }}</li>
    <li>Date de naissance : {{ $user->dob }}</li>
    <li>Sexe : {{ $user->sex }}</li>
    <li>Paroisse : {{ $user->ward }}</li>
    <li>Allergies : {{ $user->allergies }}</li>
    @if($pivot = $registration->pivot)
        <li>Option : {{ $pivot->option }}</li>
        <li>Chandail : {{ $pivot->wearable }}</li>
    @endif
</ul>
<p>Si vous devez modifier votre inscription, ou si vous devez annuler, veuillez simplement répondre à ce courriel.</p>
@if($event->health or $event->discharge or $event->rules)
    <p>Nous vous avons aussi attaché les documents qui doivent être signés par l'autorité parentale, et remis à vos dirigeants dans les plus brefs délais.</p>
    <p>Il est important de notter que sans ces documents, vous ne pourrez pas participer à l'activité !</p>
@endif
<p><strong>Vous devez assister au pré-camp le 7 mai</strong> et rapporter les feuilles de règlement et consentement parental signée ( jeune ,parent,évêque ) ainsi que votre paiement </p>
<p>Finallement, n'oubliez pas d'apporter votre chaise de camping, vos écritures, un sac de couchache, votre oreiller, et le matériel nécessaire pour votre niveau.</p>
<p>On a hâte de vous voir !</p>