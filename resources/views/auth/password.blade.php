<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="../../../css/login.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<form method="POST" action="/password/email">
    {!! csrf_field() !!}
    <div class="container">
        <div class="omb_login">
            <h3 class="omb_authTitle">Mot de passe oublié</h3>
            <div class="row omb_row-sm-offset-3">
                <div class="col-xs-12 col-sm-6">
                    <form class="omb_loginForm" action="" autocomplete="off" method="POST">
                        <p>Vous recevrez les instructions de réinitialisation par courriel. Veuillez indiquer votre adresse courriel</p>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control" name="email" placeholder="adresse courriel" value="{{ old('email') }}">
                        </div>
                        <span class="help-block"></span>
                        <br>

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Réinitialiser mot de passe</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>