<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/login.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <form action="/auth/register" method="POST">
      {!! csrf_field() !!}
      <div class="container">
        <div class="omb_login">
          <h3 class="omb_authTitle">Créez un compte</h3>
          <div class="row omb_row-sm-offset-3">
            <div class="col-xs-12 col-sm-6">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" name="name" placeholder="votre nom" value="{{ old('name') }}">
              </div>
              <span class="help-block"></span>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" name="email" placeholder="adresse courriel" value="{{ old('email') }}">
              </div>
              <span class="help-block"></span>
              
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input  type="password" class="form-control" name="password" placeholder="mot de passe">
              </div>
              <span class="help-block"></span>
              
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input  type="password" class="form-control" name="password_confirmation" placeholder="confirmez votre mot de passe">
              </div>
              <br>
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <span class="help-block">{{ $error }}</span>
                  @endforeach
                </ul>
              </div>
              @endif
              <button class="btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
            </div>
          </div>
          <br>
          <div class="row omb_row-sm-offset-3">
            <div class="col-xs-6 col-sm-3">
              <label class="checkbox">
                <input type="checkbox" value="remember-me">Se souvenir de moi !
              </label>
            </div>
            <div class="col-xs-6 col-sm-3">
              <p class="omb_forgotPwd">
                <a href="#">Oublié votre mot-de-passe ?</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </form>
  </body>
</html>
