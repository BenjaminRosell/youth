        <div class="footer-container">          
            <footer class="short footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <span class="text-white">© Copyright 2014 JeuneSDJ.com</span>
                        </div>
            
                        <div class="col-sm-9 text-right">
                            <ul class="menu">
                                <li><a href="/auth/register">Créer un compte</a></li>
                                <li><a href="/auth/login">S'identifier</a></li>
                                <li><a href="#">Instagram</a></li>
                                <li><a href="https://www.facebook.com/jeunesdupieudelongueuilquebe">Facebook</a></li>
                                <li><a class="inner-link back-to-top" href="#top">Back To Top&nbsp;<i class="icon pe-7s-angle-up-circle"></i></a></li>
                            </ul>
                        </div>
        
                    </div><!--end of row-->
                </div><!--end of container-->
            </footer>
        </div>
                
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/skrollr.min.js"></script>
        <script src="js/spectragram.min.js"></script>
        <script src="js/flexslider.min.js"></script>
        <script src="js/jquery.plugin.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/twitterfetcher.min.js"></script>
        <script src="js/placeholders.min.js"></script>
        <script src="js/scripts.js"></script>

        @yield('javascript')
    </body>
</html>