@extends('app')

@section('event')

        @if(isset($events) and $events->first())
            <input type="hidden" id="hashtag" value="{{$events->first()->hashtag}}"/>
            <input type="hidden" id="venue" value="{{$events->first()->venue->fullAddress}}"/>
        @endif

        <div class="main-container"><a id="home" class="in-page-link"></a>


                @include('partials.eventHero')

            @if(isset($events) and $events->first())
            <section class="topics duplicatable-content" id="eventInfo">

                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 align="center">Informations sur l'activité</h1>
                        </div>
                    </div><!--end of row-->

                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="topic">
                                <i class="pe-7s-clock"></i>
                                <h3>Heure d'arrivée</h3>
                                <p class="lead">
                                    Nous vous attendons sur place pour <strong>9h du matin</strong>. Soyez à l'heure !
                                </p>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="topic">
                                <i class="pe-7s-plane icon-large"></i>
                                <h3>Fin de l'activité</h3>
                                <p class="lead">
                                    Nous prévoyons la fin de l'activité pour le samedi <strong>9 juillet à midi</strong>
                                </p>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="topic">
                                <i class="pe-7s-cash icon-large"></i>
                                <h3>Prix d'entrée&nbsp;</h3>
                                <p class="lead">
                                    Le tarif pour chaque jeune fille est de 125 $. Le tarif pour les adultes est de 50 $
                                </p>

                            </div>
                        </div><!--end of individual topic-->
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="topic">
                                    <i class="pe-7s-portfolio"></i>
                                    <h3>Materiel de camps</h3>
                                    <p class="lead">
                                        Vous recevrez les déétails du matériel nécessaire <strong>par courriel</strong>
                                    </p>

                                </div>
                            </div><div class="col-md-4 col-sm-6">
                                <div class="topic">
                                    <i class="pe-7s-sun icon-large"></i>
                                    <h3>Maillots de bain</h3>
                                    <p class="lead">
                                        Pour les filles, <strong>le maillot 1 pièce ou tankini pudique</strong>
                                    </p>

                                </div>
                            </div><div class="col-md-4 col-sm-6">
                                <div class="topic">
                                    <i class="pe-7s-user icon-large"></i>
                                    <h3>Âge de participation&nbsp;</h3>
                                    <p class="lead">
                                        Le camps est destiné aux jeunes filles âgés entre <strong>12 à 18 ans</strong> et adultes acompagnateurs
                                    </p>

                                </div>
                            </div><!--end of individual topic-->

                            <!--end of individual topic-->

                            <!--end of individual topic-->
                        </div><!--end of row-->

                    </div><!--end of container-->

            </section>


            <section class="fullwidth-map screen">
                <div id="map" class="map-holder"></div>
            </section>

            @endif


            {{--<section class="color-blocks">--}}
                {{--<div class="color-block block-left col-md-6 col-sm-12"></div>--}}
                {{--<div class="color-block block-right col-md-6 col-sm-12"></div>--}}

                {{--<div class="container">--}}
                    {{--<div class="row">--}}
                        {{--<a href="#" class="block-content">--}}
                            {{--<div class="col-md-2 col-sm-4 text-center">--}}
                                {{--<i class="icon pe-7s-cash"></i>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-4 col-sm-8">--}}
                                {{--<h1>Purchase an early bird ticket and save!</h1>--}}
                                {{--<p>--}}
                                    {{--Catch our Early Bird special and save 20% off your ticket, be quick before they sell out!--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</a>--}}

                        {{--<a href="#" class="block-content">--}}
                            {{--<div class="col-md-2 col-sm-4 text-center">--}}
                                {{--<i class="icon pe-7s-phone"></i>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-4 col-sm-8">--}}
                                {{--<h1>Download the Meetup app and stay informed!</h1>--}}
                                {{--<p>--}}
                                    {{--Available on iOS and Android, the Meetup app includes directions, schedule info and more!--}}
                                {{--</p>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--</div><!--end of container-->--}}
            {{--</section>--}}

            <a id="mapping" class="in-page-link"></a>


            <a id="register" class="in-page-link"></a>

            {{--<section class="image-with-text background-dark preserve-3d">--}}
                {{----}}
                {{--<div class="container vertical-align">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-5 col-sm-8">--}}
                            {{--<h1 class="text-white">Register your attendance at Meetup 2014 while places last</h1>--}}
                            {{--<form class="register email-form">--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<input class="form-name validate-required" type="text" placeholder="Name" name="name">--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<input class="form-email validate-email" type="text" placeholder="Email Address" name="email">--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<input class="form-phone" type="text" placeholder="Phone Number" name="phone">--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<div class="select-holder">--}}
                                        {{--<select class="form-selection validate-required" name="ticket">--}}
                                            {{--<option value="">Ticket Type</option>--}}
                                            {{--<option value="Standard Pass">Standard Pass</option>--}}
                                            {{--<option value="Silver Pass">Silver Pass</option>--}}
                                            {{--<option value="Gold Pass">Gold Pass</option>--}}
                                            {{--<option value="Platinum Pass">Platinum Pass</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{----}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<input type="submit" value="Register Now" class="btn">--}}
                                    {{----}}
                                {{--</div>--}}
                                {{----}}
                                {{--<div class="col-sm-12">--}}
                                    {{--<span>* We process using a 100% secure gateway</span>--}}
                                    {{--<div class="form-success"><span>Thank you, your registration has been received!</span></div>--}}
                                    {{--<div class="form-error"><span>Validation error, please check all fields</span></div>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div><!--end of row-->--}}
        {{----}}
                {{--</div><!--end of container-->--}}
                {{----}}
                {{--<div class="go-right side-image col-sm-4 col-md-6">--}}
                    {{--<div class="background-image-holder">--}}
                        {{--<img class="background-image" alt="About Image" src="img/side2.jpg">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{----}}
            {{--</section>--}}

            <section class="instagram preserve-3d">

                <div class="instafeed" data-user-name="insideenvato">
                    <ul></ul>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div>
                                <i class="icon social_instagram text-white"></i>
                                <h1 class="text-white">Des souvenirs de notre dernière activité</h1>
                                <span class="lead text-white">N'oubliez pas de publier vos photos sur <strong><a class="text-white" href="#">Instagram</a></strong></span>
                            </div>
                        </div>
                    </div><!--end of row-->
                </div><!--end of container-->
            </section>

            <?php if(isset($events) and $events->first()): ?>

                <section class="strip-divider call-to-action">
                        <div class="background-image-holder parallax-background">
                            <img class="background-image" alt="BG Image" src="img/grey-bg.jpg">
                        </div>

                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <h1>Qu'attendez-vous!? Inscrivez-vous dès maintenant !</h1>
                                    <span class="uppercase">Date limite d'inscription : 30 Avril 2016</span>
                                    @if( $isLoggedIn )
                                        @if($events->first()->userIsRegistered())
                                            <a href="#" class="btn btn-lg various fancybox.iframe" >Vous êtes inscrit !</a>
                                        @else
                                            <a href="/event/{{$events->first()->id}}/register" class="btn btn-lg various fancybox.iframe hidden-sm hidden-xs">Incrivez-vous</a>
                                            <a href="/event/{{$events->first()->id}}/register" class="btn btn-lg hidden-md hidden-lg">Incrivez-vous</a>
                                        @endif
                                    @else
                                        <a href="/event/{{$events->first()->id}}/register" class="btn btn-lg">Incrivez-vous</a>
                                    @endif
                                    <span class="uppercase">Oh, n'oubliez pas d'inviter vos amis !</span>
                                    <a href="https://www.facebook.com/jeunesdupieudelongueuilquebec"><i class="icon social_facebook"></i></a>
                                    <a href="#"><i class="icon social_twitter"></i></a>
                                </div>
                            </div><!--end of row-->

                        </div><!--end of container-->
                </section>

            <?php endif; ?>
        </div>
@stop

@section('javascript')
    <script src="//maps.google.com/maps/api/js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.12/gmaps.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".various").fancybox({
                maxWidth    : 800,
                maxHeight   : 600,
                fitToView   : false,
                width       : '70%',
                height      : '70%',
                autoSize    : false,
                closeClick  : false,
                openEffect  : 'none',
                closeEffect : 'none'
            });

            var map = new GMaps({
                el: '#map',
                lat: -12.043333,
                lng: -77.028333,
                zoom: 12
            });

            GMaps.geocode({
                address: $('#venue').val(),
                callback: function(results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                        map.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng()
                        });
                    }
                }
            });

        });
    </script>
@stop