        <div class="nav-container">
                <nav class="overlay-nav">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2">
                                <a href="index.html">
                                    <img alt="Logo" class="logo logo-light" src="img/logo.png">
                                    <img alt="Logo" class="logo logo-dark" src="img/logo-dark.png">
                                </a>
                            </div>
                    
                            <div class="col-md-10 text-right">
                                <ul class="menu">

                                    @if(Auth::check())
                                        <li class="has-dropdown">
                                            <a class="inner-link" href="#register" target="default">
                                                {{$currentUser->name}}
                                            </a>
                                            <ul class="nav-dropdown">
                                                {{--<li><a href="profile">Mon profil</a></li>--}}
                                                <li><a href="auth/logout">Déconnexion</a></li>
                                            </ul>
                                        </li>
                                        @if( $currentUser->admin )
                                            <li><a class="inner-link" href="admin/events" target="default">Administration</a></li>
                                        @endif
                                    @else
                                        <li><a class="inner-link" href="auth/login">S'identifier</a></li>
                                    @endif
                                    {{--<li class="social-link"><a href="#"><i class="icon social_twitter"></i></a></li>--}}
                                    <li class="social-link"><a href="https://www.facebook.com/jeunesdupieudelongueuilquebec" target="_blank"><i class="icon social_facebook"></i></a></li>
                                    {{--<li class="social-link instagram-toggle"><a href="#" class="instagram-toggle-init"><i class="icon social_instagram"></i></a></li>--}}
                                </ul>
                                {{--<div class="sidebar-menu-toggle"><i class="icon icon_menu"></i></div>--}}
                                <div class="mobile-menu-toggle"><i class="icon icon_menu"></i></div>
                            </div>
                        </div><!--end of row-->
                    </div><!--end of container-->
            
                    <div class="bottom-border"></div>
            
                    <div class="sidebar-menu">
                        <img alt="Logo" class="logo" src="img/logo.png">
                        <div class="bottom-border"></div>
                        <div class="sidebar-content">
                    
                            <div class="widget">
                                <ul class="menu">
                                    <li><a class="inner-link" href="#home" target="default">home</a></li>
                                    
                                    <li><a class="inner-link" href="#speakers" target="default">speakers</a></li><li><a class="inner-link" href="#schedule" target="default">schedule</a></li><li><a class="inner-link" href="#pricing" target="default">pricing</a></li><li><a class="inner-link" href="#register" target="default">register</a></li>
                                    <li class="social-link"><a href="#"><i class="icon social_twitter"></i></a></li>
                                    <li class="social-link"><a href="#"><i class="icon social_facebook"></i></a></li>
                                    <li class="social-link instagram-toggle"><a href="#" class="instagram-toggle-init"><i class="icon social_instagram"></i></a></li>
                                </ul>
                            </div>
                    
                            <div class="widget">
                                <ul class="social-profiles">
                                    <li><a href="#"><i class="icon social_twitter"></i></a></li>
                                    <li><a href="#"><i class="icon social_facebook"></i></a></li>
                                    <li><a href="#"><i class="icon social_dribbble"></i></a></li>
                                    <li><a href="#"><i class="icon social_instagram"></i></a></li>
                                    <li><a href="#"><i class="icon social_googleplus"></i></a></li>
                                </ul>
                            </div>
                    
                            <div class="copy-text">
                                <span>© Copyright 2014 Meetup Inc.</span>
                            </div>
                        </div><!--end of sidebar content-->
                    </div><!--end of sidebar-->
                </nav>
                
        </div>