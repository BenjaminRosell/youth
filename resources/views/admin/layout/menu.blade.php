<nav>
    <div class="nav-wrapper teal darken-3">
        <a href="#!" class="brand-logo">Jeunes SDJ</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="/admin/users"><i class="mdi-social-person left"></i>Utilisateurs</a></li>
            <li><a href="/admin/events"><i class="mdi-action-today left"></i>Activités</a></li>
            <li><a href="/admin/venues"><i class="mdi-maps-place left"></i>Emplacements</a></li>
            <li><a href="/"><i class="mdi-hardware-keyboard-arrow-right right"></i>Retourner au site</a></li>
        </ul>
        <ul class="side-nav" id="mobile-demo">
            <li><a href="/admin/users"><i class="mdi-social-person left"></i>Utilisateurs</a></li>
            <li><a href="/admin/events"><i class="mdi-action-today left"></i>Activités</a></li>
            <li><a href="/admin/venues"><i class="mdi-maps-place left"></i>Emplacements</a></li>
            <li><a href="/"><i class="mdi-hardware-keyboard-arrow-right right"></i>Voir le site</a></li>
        </ul>
    </div>
</nav>