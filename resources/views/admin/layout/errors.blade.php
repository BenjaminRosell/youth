@if (count($errors) > 0)
    <div class="row">
        <div class="col s12">
            <div class="card-panel red">
                <span class="white-text">Vous avez oublié de remplir quelques champs...</span>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li class="white-text">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif