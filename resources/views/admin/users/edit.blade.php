@extends('admin.app')

@section('content')
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text">
                <img src="{{ $user->avatar ?: $user->gravatar }}" class="circle"/>    {{ $user->name }}
            </h1>
            <pr></pr>
        </div>
    </div>

    <div class="container">

       @include('admin.layout.errors')

        <div class="row">
            <form action="/admin/users/{{$user->id}}" method="POST" class="col s12">
                <input type="hidden" name="_method" value="PUT">
                @include('admin.users.form')
            </form>
        </div>
    </div>
    <input type="hidden" id="oldType" value="{{$user->type}}"/>
    <input type="hidden" id="oldSex" value="{{$user->sex}}"/>
    <input type="hidden" id="oldWard" value="{{$user->ward}}"/>
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datepicker').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 25, // Creates a dropdown of 15 years to control year
                format: 'yyyy-mm-dd'
            });

            $('#type').val($('#oldType').val());
            $('#ward').val($('#oldWard').val());
            $('#sex').val($('#oldSex').val());

            $('select').material_select();
        })
    </script>
@endsection