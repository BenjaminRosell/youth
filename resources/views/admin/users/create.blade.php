@extends('admin.app')

@section('content')
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text">Nouvel emplacement</h1>
            <pr></pr>
        </div>
    </div>

    <div class="container">

        @include('admin.layout.errors')

        <div class="row">
            <form action="/admin/users" method="POST" class="col s12">
                @include('admin.users.form')
            </form>
        </div>
    </div>

@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $('.datepicker').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 40, // Creates a dropdown of 15 years to control year
                format: 'yyyy-mm-dd'
            });

            $('select').material_select();
        })
    </script>
@endsection