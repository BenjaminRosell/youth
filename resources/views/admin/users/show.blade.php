@extends('admin.app')

@section('content')

    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text">
                <img src="{{ $user->avatar ?: $user->gravatar }}" class="circle"/>    {{ $user->name }}
            </h1>
            <pr></pr>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col s12 m5">
                <div class="card">
                    <div class="card-image">
                        <img src="{{$user->avatar ?: $user->bigGravatar}}">
                    </div>
                    <div class="card-content">
                        <p class="truncate"><i class="mdi-social-person"></i> {{$user->name}}</p>
                        <p class="truncate"><i class="mdi-communication-email"></i>
                            <a href="mailto:{{$user->email}}"> {{$user->email}}</a>
                        </p>
                        <p><i class="mdi-social-location-city"></i> {{$user->ward}}</p>
                    </div>
                    <div class="card-action">
                        <a href="/admin/users">Retour</a>
                        <a href="/admin/users/{{$user->id}}/edit">Modifier</a>
                    </div>
                </div>
            </div>
            <div class="col s12 m7">
                <div class="section">
                    <h5>Informations de l'utilisateur</h5>
                    <div class="row">
                        <div class="col s6">
                            <span class="truncate"><i class="mdi-social-person"></i> {{$user->sex}}</span><br/>
                            <span class="truncate"><i class="mdi-social-people"></i> {{$user->type}}</span>
                        </div>
                        <div class="col s6">
                            <span class="truncate"><i class="mdi-action-today"></i> {{$user->dob}}</span><br/>
                            @if($user->admin)
                                <span><i class="mdi-communication-vpn-key"></i> Administrateur</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="section">
                    <h5>Activités</h5>
                    <p>Stuff</p>
                </div>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <input type="hidden" id="address" value="{{$user->fullAddress}}"/>

@endsection


@section('javascript')
    <script>
        $(document).ready(function() {

        });
    </script>
@stop