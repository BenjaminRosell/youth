@extends('admin.app')

@section('content')
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text">Gestion des utilisateurs</h1>
            <div class="row center">
                <h5 class="header col s12 light">Vous pouvez modifier toutes les données</h5>
            </div>
            <div class="row center">
                <a href="/admin/users/create" id="download-button" class="btn waves-effect waves-light orange"><i class="mdi-social-person-add left"></i>Ajouter</a>
            </div>
            <br><br>

        </div>
    </div>

    <div class="container">
        <div class="row">
            @foreach($users as $user)
                <div class="col l3 m4 s12">
                    <div class="card">
                        <div class="card-image">
                            <a href="/admin/users/{{$user->id}}">
                                <img src="{{$user->avatar ?: $user->bigGravatar}}">
                            </a>
                        </div>
                        <div class="card-content">
                            <p class="truncate"><i class="mdi-social-person"></i> {{$user->name}}</p>
                            <p class="truncate"><i class="mdi-communication-email"></i>
                                <a href="mailto:{{$user->email}}"> {{$user->email}}</a>
                            </p>
                            <p><i class="mdi-social-location-city"></i> {{$user->ward}}</p>
                        </div>
                        <div class="card-action">
                            <a href="/admin/users/{{$user->id}}/edit">Modifier</a>
                            <a href='#' class="js-delete" data-delete-id="{{$user->id}}">Supprimer</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <!-- Delete Modal -->
    <div id="deleteModal" class="modal">
        <div class="modal-content">
            <h4>Supprimer utilisateur</h4>
            <p>Êtes-vous certain de vouloir supprimer cet utilisateur ?</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-red btn-flat">Annuler</a>
            <a href="#!" class="js-delete-confirm modal-action modal-close waves-effect waves-green btn-flat">Oui, je le veux</a>
        </div>
    </div>
@endsection


@section('javascript')
    <script>
        $(document).ready(function() {

            $('.js-delete-confirm').click(function(event) {
                deleteItem(localStorage.getItem("userToDelete"));
                localStorage.removeItem("userToDelete");
            });

            $('.js-delete').click(function(event) {
                $('#deleteModal').openModal();
                localStorage.setItem("userToDelete", $(this).data('delete-id'));
            });

        });

        function deleteItem(item) {
            $.ajax({
                url: '/admin/users/' + item,
                type: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function() {
                window.location.reload()
            })
        }
    </script>
@endsection