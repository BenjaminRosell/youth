{!! csrf_field() !!}

<div class="row">
    <div class="input-field col s12">
        <input id="name" name="name" type="text" class="validate" value="{{ @$user->name ?: old('name') }}">
        <label for="name">Nom de l'utilisateur</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
        <input id="address" name="email" type="email" class="validate" value="{{ @$user->email ?: old('email') }}">
        <label for="address">Adresse courriel</label>
    </div>
    <div class="input-field col s6">
        <select id="ward" name="ward">
            <option value="">Paroisse</option>
            <option value="Alma">Alma</option>
            <option value="Chicoutimi">Chicoutimi</option>
            <option value="Drummondville">Drummondville</option>
            <option value="Granby">Granby</option>
            <option value="La Prairie">La Prairie</option>
            <option value="Lemoyne">Lemoyne</option>
            <option value="Longueuil">Longueuil</option>
            <option value="Mont-St-Hilaire">Mont-St-Hilaire</option>
            <option value="Québec">Québec</option>
            <option value="Rimouski">Rimouski</option>
            <option value="Ste-Foy">Ste-Foy</option>
            <option value="Sherbrooke">Sherbrooke</option>
            <option value="St-Jean">St-Jean</option>
            <option value="Trois-Rivières">Trois-Rivières</option>
            <option value="Victoria">Victoria</option>
            <option value="Victoriaville">Victoriaville</option>
            <option value="Pieu de Montréal">Pieu de Montréal</option>
            <option value="Mount Royal Stake">Mount Royal Stake</option>
            <option value="Autre">Autre</option>
        </select>
        <label for="city">Paroisse</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
        <input id="dob" name="dob" type="date" class="datepicker validate" value="{{ @$user->dob ?:old('dob') }}">
        <label for="province">Date de naissance</label>
    </div>
    <div class="input-field col s6">
        <select id="sex" name="sex">
            <option value="">Sexe</option>
            <option value="male">Homme</option>
            <option value="female">Femme</option>
        </select>
        <label for="postal_code">Sexe</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
        <div class="switch">
            <label>
                Administrateur
                <input type="checkbox" name="admin" value="true" {{@$user->admin ? 'checked': ''}} >
                <span class="lever"></span>
            </label>
        </div>

    </div>
    <div class="input-field col s6">
        <select id="type" name="type">
            <option value="">Choissisez votre option</option>
            <option value="youth">Jeune</option>
            <option value="parent">Parent</option>
            <option value="leader">Dirigeant</option>
        </select>
        <label>Type de compte</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s6">
        <input id="parent_email" name="parent_email" type="email" class="validate" value="{{ @$user->parent_email ?: old('parent_email') }}">
        <label for="parent_email">Adresse courriel du parent</label>
    </div>
</div>

<button class="btn waves-effect waves-light right" type="submit">Envoyer
    <i class="mdi-content-send left"></i>
</button>