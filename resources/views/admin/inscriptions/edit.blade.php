@extends('admin.app')

@section('content')
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text">
                <img src="{{ $inscription->user->avatar ?: $inscription->user->gravatar }}" class="circle" width="50px"/>    {{ $inscription->user->name }}
            </h1>
            <h5 class="header center teal-text">Détails de son inscription pour l'activité : {{$inscription->event->name}}</h5>
        </div>
    </div>

    <br/>

    <div class="container">

        @include('admin.layout.errors')

        <div class="row">
            <form action="/admin/inscriptions/{{$inscription->id}}" method="POST" class="col s12">
                <input type="hidden" name="_method" value="PUT">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="input-field col s6">
                        <select class="form-control" name="option" id="option">
                            <option value="">Choisissez un niveau</option>
                            <option value="Niveau 1">Niveau 1</option>
                            <option value="Niveau 2">Niveau 2</option>
                            <option value="Niveau 3">Niveau 3</option>
                            <option value="Niveau 4">Niveau 4</option>
                            <option value="DJ 1">DJ 1</option>
                            <option value="DJ 2">DJ 2</option>
                        </select>
                        <label for="city">Thématique</label>
                    </div>
                    <div class="input-field col s6">
                        <select id="wearable" name="wearable">
                            <option value="">Choisissez une grandeur</option>
                            <option value="Petit">Petit</option>
                            <option value="Moyen">Moyen</option>
                            <option value="Large">Large</option>
                            <option value="Extra-Large">Extra-Large</option>
                        </select>
                        <label for="city">Grandeur de chandail</label>
                    </div>
                </div>

                <button class="btn waves-effect waves-light right" type="submit">Envoyer
                    <i class="mdi-content-send left"></i>
                </button>
            </form>
        </div>
    </div>
    <input type="hidden" id="oldOption" value="{{$inscription->option}}"/>
    <input type="hidden" id="oldWearable" value="{{$inscription->wearable}}"/>
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {

            $('#option').val($('#oldOption').val());
            $('#wearable').val($('#oldWearable').val());

            $('select').material_select();
        })
    </script>
@endsection