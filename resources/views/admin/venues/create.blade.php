@extends('admin.app')

@section('content')
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text">Nouvel emplacement</h1>
            <pr></pr>
        </div>
    </div>

    <div class="container">

        @include('admin.layout.errors')

        <div class="row">
            <form action="/admin/venues" method="POST" class="col s12">
                @include('admin.venues.form')
            </form>
        </div>
    </div>

@endsection