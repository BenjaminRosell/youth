@extends('admin.app')

@section('content')
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text">Les emplacements</h1>
            <div class="row center">
                <h5 class="header col s12 light">Ajoutez un emplacement pour des activités</h5>
            </div>
            <div class="row center">
                <a href="/admin/venues/create" id="download-button" class="btn waves-effect waves-light orange"><i class="mdi-content-add left"></i>Ajouter</a>
            </div>
            <br><br>

        </div>
    </div>

    <div class="container">

        <table class="responsive-table">
            <thead>
            <tr>
                <th data-field="id">Nom</th>
                <th data-field="name">Adresse</th>
                <th data-field="price">Actions</th>
            </tr>
            </thead>

            <tbody>
                @foreach($venues as $venue)
                    <tr>
                        <td><a href="/admin/venues/{{$venue->id}}">{{$venue->name}}</a></td>
                        <td>{{$venue->fullAddress}}</td>
                        <td>
                            <a class="waves-effect waves-light btn" href="/admin/venues/{{$venue->id}}/edit">
                                <i class="mdi-editor-mode-edit"></i>
                            </a>
                            <a class="waves-effect waves-light btn" href="{{$venue->mapLink}}" target="_blank">
                                <i class="mdi-maps-map"></i>
                            </a>

                            <a class="red darken-4 waves-effect waves-light btn js-delete" data-delete-id="{{$venue->id}}">
                                <i class="mdi-action-delete"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Delete Modal -->
    <div id="deleteModal" class="modal">
        <div class="modal-content">
            <h4>Supprimer Emplacement</h4>
            <p>Êtes-vous certain de vouloir supprimer cet emplacement ?</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-red btn-flat">Annuler</a>
            <a href="#!" class="js-delete-confirm modal-action modal-close waves-effect waves-green btn-flat">Oui, je le veux</a>
        </div>
    </div>
@endsection


@section('javascript')
    <script>
        $(document).ready(function() {

            $('.js-delete-confirm').click(function(event) {
                deleteItem(localStorage.getItem("eventToDelete"));
                localStorage.removeItem("eventToDelete");
            });

            $('.js-delete').click(function(event) {
                $('#deleteModal').openModal();
                localStorage.setItem("eventToDelete", $(this).data('delete-id'));
            });

        });

        function deleteItem(item) {
            $.ajax({
                url: '/admin/venues/' + item,
                type: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function() {
                window.location.reload()
            })
        }
    </script>
@endsection