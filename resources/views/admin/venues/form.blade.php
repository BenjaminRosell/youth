{!! csrf_field() !!}

<div class="row">
    <div class="input-field col s12">
        <input id="name" name="name" type="text" class="validate" value="{{ @$venue->name ?: old('name') }}">
        <label for="name">Nom de l'emplacement</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
        <input id="address" name="address" type="text" class="validate" value="{{ @$venue->address ?: old('address') }}">
        <label for="address">Adresse Civique</label>
    </div>
    <div class="input-field col s6">
        <input id="city" name="city" type="text" class="validate" value="{{ @$venue->city ?: old('city') }}">
        <label for="city">Ville</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
        <input id="province" name="province" type="text" class="validate" value="{{ @$venue->province ?:old('province') }}">
        <label for="province">Province</label>
    </div>
    <div class="input-field col s6">
        <input id="postal_code" name="postal_code" type="text" class="validate" value="{{ @$venue->postal_code ?: old('validate') }}">
        <label for="postal_code">Code Postal</label>
    </div>
</div>

<button class="btn waves-effect waves-light right" type="submit">Envoyer
    <i class="mdi-content-send left"></i>
</button>