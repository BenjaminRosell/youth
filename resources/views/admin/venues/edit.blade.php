@extends('admin.app')

@section('content')
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text">{{ $venue->name }}</h1>
            <pr></pr>
        </div>
    </div>

    <div class="container">

        @include('admin.layout.errors')

        <div class="row">
            <form action="/admin/venues/{{$venue->id}}" method="POST" class="col s12">
                <input type="hidden" name="_method" value="PUT">
                @include('admin.venues.form')
            </form>
        </div>
    </div>

@endsection