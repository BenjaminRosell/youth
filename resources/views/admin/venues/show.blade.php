@extends('admin.app')

@section('content')

    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text">{{$venue->name}}</h1>
            <pr></pr>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col s12 m12">
                <div class="card">
                    <div class="card-image">
                        <div id="map" style="min-height: 300px;"></div>
                    </div>
                    <div class="card-content">
                        <p><i class="mdi-maps-place"></i> {{$venue->fullAddress}} </p>
                    </div>
                    <div class="card-action">
                        <a href="/admin/venues">Retour</a>
                        <a href="/admin/venues/{{$venue->id}}/edit">Modifier</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="address" value="{{$venue->fullAddress}}"/>

@endsection


@section('javascript')
    <script src="//maps.google.com/maps/api/js?sensor=true"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.12/gmaps.min.js"></script>
    <script>
        $(document).ready(function() {

            var map = new GMaps({
                el: '#map',
                lat: 45.143333,
                lng: -73.028333,
                zoom: 12
            });


            GMaps.geocode({
                address: $('#address').val(),
                callback: function(results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                        map.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng()
                        });
                    }
                }
            });

        });
    </script>
@stop