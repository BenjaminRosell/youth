
<!DOCTYPE html>
<html>
<head>
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.96.1/css/materialize.min.css">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style>
        html {font-size: 80%;}
        @media only screen and (min-width: 993px) {
            .container {
                width: 85%;
            }
        }
    </style>
    @yield('styles')

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
    @include('admin.layout.menu')

    @yield('content')


<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.96.1/js/materialize.min.js"></script>
<script>
    $(document).ready(function() {
        $(".button-collapse").sideNav();
    });
</script>
@yield('javascript')
</body>
</html>
