@extends('admin.app')

@section('content')
    @include('admin.layout.datatables')
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text">
                {{ $event->name }}
            </h1>
            <pr></pr>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col s12 m12">
                <div class="card">
                    <div class="card-image">
                        <div id="map" style="min-height: 300px;"></div>
                    </div>
                    <input type="hidden" id="address" value="{{$event->venue->fullAddress}}"/>
                    <div class="card-content">
                        <p class="truncate"><i class="mdi-social-person"></i> {{$event->name}}</p>
                        <p class="truncate"><i class="mdi-maps-place"></i>
                            {{$event->venue->fullAddress}}
                        </p>
                        <p><i class="mdi-action-today"></i> {{$event->starts_at->formatLocalized('%A %d %B %Y')}}</p>
                    </div>
                    <div class="card-action">
                        <a href="/admin/events">Retour</a>
                        <a href="/admin/events/{{$event->id}}/edit">Modifier</a>
                    </div>
                </div>
            </div>
            <div class="col s12 m12">
                <div class="section">
                    <h5>Participants ({{$event->youth->count()}})</h5>
                    @include('admin.events.partials.table', ['participants' => $event->youth])

                    <br>
                    <br>
                    <br>

                    <h5>Parents ({{$event->parents->count()}})</h5>
                    @include('admin.events.partials.table', ['participants' => $event->parents])

                    <br>
                    <br>
                    <br>

                    <h5>Dirigeants ({{$event->leaders->count()}})</h5>
                    @include('admin.events.partials.table', ['participants' => $event->leaders])

                    <br>
                </div>
                <div class="divider"></div>
            </div>
        </div>
    </div>

    <input type="hidden" id="address" value="{{$currentUser->fullAddress}}"/>

    <div id="deleteModal" class="modal">
        <div class="modal-content">
            <h4>Supprimer l'inscription</h4>
            <p>Êtes-vous certain de vouloir supprimer cette inscription ?</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-red btn-flat">Annuler</a>
            <a href="#!" class="js-delete-confirm modal-action modal-close waves-effect waves-green btn-flat">Oui, je le veux</a>
        </div>
    </div>

@endsection


@section('javascript')
    <script src="//maps.google.com/maps/api/js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.12/gmaps.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {

            $('table').DataTable({
                "paging": false
            } );

            var map = new GMaps({
                el: '#map',
                lat: 45.143333,
                lng: -73.028333,
                zoom: 12
            });


            GMaps.geocode({
                address: $('#address').val(),
                callback: function(results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                        map.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng()
                        });
                    }
                }
            });

            $('.js-delete-confirm').click(function(event) {
                deleteItem(localStorage.getItem("userToDelete"));
                localStorage.removeItem("userToDelete");
            });

            $('.js-delete').click(function(event) {
                $('#deleteModal').openModal();
                localStorage.setItem("userToDelete", $(this).data('delete-id'));
            });

        });

        function deleteItem(item) {
            $.ajax({
                url: '/admin/inscriptions/' + item,
                type: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function() {
                window.location.reload()
            })
        }


    </script>
@stop
