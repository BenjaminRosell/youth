<table class="hoverable">
    <thead>
        <tr>
            <th width="15%" data-field="name">Nom</th>
            <th width="10%" data-field="ward">Paroisse</th>
            <th width="10%" data-field="age">Age</th>
            <th width="10%" data-field="price">Niveau</th>
            <th width="15%" data-field="price">Deja fait</th>
            <th width="10%" data-field="price">Chandail</th>
            <th width="15%" data-field="price">Allergies</th>
            <th width="15%" data-field="price">Actions</th>
        </tr>
    </thead>

    <tbody>
        @foreach($participants as $participant)
            <tr>
                <td>
                    <img src="{{ $participant->avatar ?: $participant->gravatar }}" height="25px" width="25px" class="circle" alt="{{$participant->name}} "/>
                    {{$participant->name}}
                </td>
                <td>{{$participant->ward}}</td>
                <td>{{\Carbon\Carbon::parse($participant->dob)->diffInYears()}}</td>
                <td>{{$participant->pivot->option}}</td>
                <td>{{$participant->pivot->notes}}</td>
                <td>{{$participant->pivot->wearable}}</td>
                <td>{{$participant->allergies}}</td>
                <td>
                    <a class="waves-effect waves-light btn" href="/admin/inscriptions/{{ $participant->pivot->id }}/edit">
                        <i class="mdi-editor-mode-edit"></i>
                    </a>
                    <a class="red darken-4 waves-effect waves-light btn js-delete" data-delete-id="{{ $participant->pivot->id }}">
                        <i class="mdi-action-delete"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<br/>
