@extends('admin.app')

@section('styles')
    <link rel="stylesheet" href="../../../../components/trumbowyg/dist/ui/trumbowyg.min.css">
@endsection

@section('content')
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text">Nouvelle activité</h1>
            <pr></pr>
        </div>
    </div>

    <div class="container">

        @include('admin.layout.errors')

        <div class="row">
            <form action="/admin/events" method="POST" class="col s12">
                @include('admin.events.form')
            </form>
        </div>
    </div>

@endsection

@section('javascript')
    <script src="../../../../components/trumbowyg/dist/trumbowyg.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.datepicker').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 40, // Creates a dropdown of 15 years to control year
                format: 'yyyy-mm-dd',
                formatSubmit: 'yyyy-mm-dd',
                closeOnSelect: true,
                close: 'Fermer',
                monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthsShort: ['Jan', 'Fev', 'Mar', 'Avl', 'Mai', 'Jun', 'Jul', 'Auo', 'Sep', 'Oct', 'Nov', 'Dec'],
                weekdaysFull: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                clear: 'Effacer',
                today: 'Aujourd\'hui',
                labelMonthNext: 'Mois prochain',
                labelMonthPrev: 'Mois antérieur',
                labelMonthSelect: 'Choissisez un mois',
                labelYearSelect: 'Choissisez une année',
            });

            $('select').material_select();

            $('#details, #rules').trumbowyg();
        });
    </script>
@endsection