{!! csrf_field() !!}

<div class="row">
    <div class="input-field col s12">
        <input id="name" name="name" type="text" class="validate" value="{{ @$event->name ?: old('name') }}">
        <label for="name">Nom de l'activité</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <textarea id="description" name="description" class="materialize-textarea validate">{{ @$event->description ?: old('description') }}</textarea>
        <label for="description">Description</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
        <input id="image_url" name="image_url" type="text" class="validate" value="{{ @$event->image_url ?: old('image_url') }}">
        <label for="image_url">URL de l'image de fond</label>
    </div>
    <div class="input-field col s6">
        <input id="hashtag" name="hashtag" type="text" class="validate" value="{{ @$event->hashtag ?: old('hastag') }}">
        <label for="hashtag">Hashtag</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <select id="venue" name="venue_id">
            <option value="">Emplacement</option>
            @foreach($venues as $venue)
                <option value="{{$venue->id}}">{{$venue->name}}</option>
            @endforeach
        </select>
        <label for="city">Paroisse</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
        <input id="starts_at" name="starts_at" type="date" class="datepicker validate" value="{{ @$event->starts_at ?:old('dob') }}">
        <label for="starts_at">Date de debut</label>
    </div>
    <div class="input-field col s6">
        <input id="ends_at" name="ends_at" type="date" class="datepicker validate" value="{{ @$event->ends_at ?:old('ends_at') }}">
        <label for="ends_at">Date de fin</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <textarea id="rules" name="rules"></textarea>
        <label for="rules">Reglements</label>
    </div>
</div>
<div class="row">
    <div class="input-field col s6">
        <div class="switch">
            <label>
                Authorization parentale
                <input type="checkbox" name="discharge" value="true" {{@$event->discharge ? 'checked': ''}} >
                <span class="lever"></span>
            </label>
        </div>
    </div>
    <div class="input-field col s6">
        <div class="switch">
            <label>
                Formulaire médical
                <input type="checkbox" name="health" value="true" {{@$event->health ? 'checked': ''}} >
                <span class="lever"></span>
            </label>
        </div>
    </div>
</div>

<br/><br/><br/>
<button class="btn waves-effect waves-light left" type="submit">Envoyer
    <i class="mdi-content-send left"></i>
</button>