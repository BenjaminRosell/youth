            <section class="hero-slider">
                <ul class="slides">

                    @forelse ($events as $event)
                    
                        <li class="hero-slide">
                            <div class="background-image-holder parallax-background">
                                <img class="background-image" alt="Background Image" src="{{$event->image_url}}">
                            </div>
                        
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 col-md-offset-1 col-sm-offset-1 center-block">
                                        <img src="../../../img/wheel_conf_jeun.png" alt="" class="img-responsive image-wheel"/>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <h1 class="large-h1 text-white"><strong>{{$event->name}}</strong></h1>
                                        <span class="lead text-white">{{$event->description}}</span>
                                        <br>
                                        <br/><span class="text-white"><i class="fa fa-calendar"></i> &nbsp;{{$event->starts_at->formatLocalized('%d %B %Y')}}&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-map-marker"></i> &nbsp;{{$event->venue->partialAddress}}&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-tag"></i> &nbsp;#{{$event->hashtag}}
                                        <br><br>
                                        <p>
                                            @if( $isLoggedIn )
                                                @if($event->userIsRegistered())
                                                    <a href="#" class="btn various fancybox.iframe" >Vous êtes inscrit !</a>
                                                @else
                                                    <a href="/event/{{$event->id}}/register" class="btn various fancybox.iframe hidden-sm hidden-xs">Inscription</a>
                                                    <a href="/event/{{$event->id}}/register" class="btn hidden-md hidden-lg">Inscription</a>
                                                @endif
                                            @else
                                                <a href="/event/{{$event->id}}/register" class="btn">Inscription</a>
                                            @endif
                                            <a href="#eventInfo" class="btn btn-hollow hidden-xs hidden-sm">Informations</a>
                                        </p>
                                    </div>

                                </div><!--end of row-->
                            </div><!--end of container-->
                        </li>
                    @empty
                        @include('partials.noEvent')
                    @endforelse
                </ul>
            </section>