@include('layout.header')

<style>
html, body {
  height: 100%;
}

.vertical-center {
  margin-bottom: 0; /* Remove the default bottom margin of .jumbotron */
}

.vertical-center {
  min-height: 100%;  /* Fallback for vh unit */
  min-height: 100vh; /* You might also want to use
                        'height' property instead.
                        
                        Note that for percentage values of
                        'height' or 'min-height' properties,
                        the 'height' of the parent element
                        should be specified explicitly.
  
                        In this case the parent of '.vertical-center'
                        is the <body> element */

  /* Make it a flex container */
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex; 
  
  /* Align the bootstrap's container vertically */
    -webkit-box-align : center;
  -webkit-align-items : center;
       -moz-box-align : center;
       -ms-flex-align : center;
          align-items : center;
  
  /* In legacy web browsers such as Firefox 9
     we need to specify the width of the flex container */
  width: 100%;
  
  /* Also 'margin: 0 auto' doesn't have any effect on flex items in such web browsers
     hence the bootstrap's container won't be aligned to the center anymore.
  
     Therefore, we should use the following declarations to get it centered again */
         -webkit-box-pack : center;
            -moz-box-pack : center;
            -ms-flex-pack : center;
  -webkit-justify-content : center;
          justify-content : center;
}

.container {
  background-color: #00936b;
}

h1, h2, h3, p, span  {
    color: white !important;
}
h1  {
    margin-bottom: 10px;
}
a.blue {
    color: #3b5998 !important;
}

 i {
  color: #00936b;
  font-size: 70px;
  display: inline-block;
  border: 2px solid #fff;
  border-radius: 50%;
  width: 120px;
  height: 120px;
  line-height: 117px !important;
  background: #fff;
  text-align: center;
  transition: all 0.1s ease-out;
  -webkit-transition: all 0.1s ease-out;
  -moz-transition: all 0.1s ease-out;
}

</style>


<div class="container vertical-center">
    <div class="row">
        <div class="col-md-4 col-sm-4 text-center">
            <i class="icon pe-7s-check"></i>
        </div>
        <div class="col-md-8 col-sm-8">
            <h1>Félicitations ! Vous êtes maintenant enregistré pour : </h1>
            <h3>{{$event->name}} </h3>
            <p>
                Vous recevrez sous peu un courriel de confirmation avec des documents que vous devez signer et nous faire parvenir. Votre inscription ne sera validée qu'après reception de ces documents.
            </p>
            <p>
                N'oubliez pas d'inviter vos amis, et de publier en utilisant <a href="https://www.facebook.com/search/str/%23{{$event->hashtag}}/keywords_top" class="blue" target="_blank">#{{$event->hashtag}}</a>
            </p>
        </div>
    </div>
</div>

<script>
    setTimeout(function(){ window.top.location.href = "/"; }, 6000);
</script>