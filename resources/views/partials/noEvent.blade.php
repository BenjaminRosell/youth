<li class="hero-slide">
    <div class="background-image-holder parallax-background">
        <img class="background-image" alt="Background Image" src="img/hero8.jpg">
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <img alt="Meetup 2014" class="logo" src="img/logo.png">
                <h1 class="large-h1 text-white">Nous n'avons pas d'évenements à venir prochainement</h1>
                <span class="lead text-white">We're gonna make one of those blurry cafe videos and everything, so don't miss out!</span>
                <p>
                    <a href="/auth/login">Inscription</a>
                </p>
            </div>

        </div><!--end of row-->
    </div><!--end of container-->
</li>