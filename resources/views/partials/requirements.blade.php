<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="../../../css/login.css">
    <link rel="stylesheet" href="../../../css/login.css">
    <link rel="stylesheet" href="../../../css/datepicker.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<form action="../../../event/{{$event->id}}/requirements" method="POST">
    {!! csrf_field() !!}
    <div class="container">
        <div class="omb_login">
            <h3 align="center">Avant de vous inscrire ...</h3>
            <h5 class="omb_authTitle">Prenez quelques instants pour compléter les indications suivantes ...</h5>
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    @if($event->health)
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="health" id="health"> Vous recevrez <strong>une fiche de santé</strong> par courriel que vous devrez remettre à vos dirigeants avant la date de l'activité.
                            </label>
                        </div>
                        <hr/>
                    @endif

                    @if($event->discharge)
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="discharge"  id="discharge"> Vous recevrez <strong>une décharge parentale</strong> par courriel que vous devrez remettre à vos dirigeants avant la date de l'activité.
                            </label>
                        </div>
                            <hr/>
                        {{--@if($currentUser->type == 'youth')--}}
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="activity" id="activity"> Vous devez <strong>choisir votre niveau</strong> pour le camps des jeunes filles 2016 parmis les choix suivants :
                                </label>
                            </div>

                            <div class="form-group">
                                <select class="form-control" name="option" id="option">
                                    <option value="">Choisissez un niveau</option>
                                    <option value="Niveau 1">Niveau 1</option>
                                    <option value="Niveau 2">Niveau 2</option>
                                    <option value="Niveau 3">Niveau 3</option>
                                    <option value="Niveau 4">Niveau 4</option>
                                    <option value="DJ 1">DJ 1</option>
                                    <option value="DJ 2">DJ 2</option>
                                    <option value="Option totem">Option totem</option>
                                    <option value="Dirigeante">Dirigeante</option>
                                </select>
                            </div>
                            <hr/>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="activity" id="activity"> Assurez-vous de répondre à la question suivante : Quels niveaux de jeunes filles avez vous déjà accomplit auparavant :
                                </label>
                            </div>

                            <div class="form-group">
                                <select class="selectpicker" name="notes[]" id="option" multiple title='Niveaux déjà complétés...'>
                                    <option value="Aucun">Aucun</option>
                                    <option value="Niveau 1">Niveau 1</option>
                                    <option value="Niveau 2">Niveau 2</option>
                                    <option value="Niveau 3">Niveau 3</option>
                                    <option value="Niveau 4">Niveau 4</option>
                                    <option value="DJ 1">DJ 1</option>
                                    <option value="DJ 2">DJ 2</option>
                                    <option value="Dirigeante">Dirigeante</option>
                                </select>
                            </div>
                            <hr/>
                        {{--@endif--}}
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="wearableCheck" id="wearableCheck"> Vous recevrez aussi un chandail avec la thématique de l'activité. Quelle grandeur portez-vous ? :
                            </label>
                        </div>

                        <div class="form-group">
                            <select class="form-control" name="wearable" id="wearable">
                                <option value="">Choisissez une grandeur</option>
                                <option value="Petit">Petit</option>
                                <option value="Moyen">Moyen</option>
                                <option value="Large">Large</option>
                                <option value="Extra-Large">Extra-Large</option>
                            </select>
                        </div>
                            <hr/>
                    @endif

                    @if($event->rules)
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="rules" id="rules"> J'ai lu et je m'engage à respecter les règlements de l'activité en tout moment.
                            </label>
                        </div>

                        {!! $event->rules !!}

                    @endif

                    <br/><br/><br/>

                    <button class="btn btn-lg btn-primary btn-block" id="submit" type="submit">Sauvegarder</button>

                    <span align="center" class="help-block error" id="error"></span>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <span class="help-block">{{ $error }}</span>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</form>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
<script src="../../../js/bootstrap.min.js"></script>
<script src="../../../js/datepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
<script>
    $(document).ready(function() {

        $('.selectpicker').selectpicker({width: "100%"});

        $('#submit').click(function(e) {

            if (
                    @if($currentUser->type == 'youth')
                        $('#activity').prop('checked') != true ||
                    @endif
                    $('#discharge').prop('checked') != true ||
                    $('#health').prop('checked') != true || $('#rules').prop('checked') != true || $('#wearableCheck').prop('checked') != true ||
                    $('#option').val() == '' || $('#wearable').val() == '') {

                $('#error').text('Vous devez lire et cocher tous les éléments de la liste...');

                return false;
                e.preventDefault();
            }
        })

    });
</script>
</html>