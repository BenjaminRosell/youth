@include('layout.header')

        <div class="loader">
            <div class="strip-holder">
                <div class="strip-1"></div>
                <div class="strip-2"></div>
                <div class="strip-3"></div>
            </div>
        </div>
        
        <a id="top"></a>
                
        @include('layout.menu')
        @yield('event')


@include('layout.footer')