<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/login.css">
    <link rel="stylesheet" href="../css/login.css">
    <link rel="stylesheet" href="../css/datepicker.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <form action="../../../profile/update" method="POST">
        {!! csrf_field() !!}
        <div class="container">
            <div class="omb_login">
                <h3 align="center">Avant de vous inscrire ...</h3>
                <h5 class="omb_authTitle">Nous avons besoin d'un peu plus d'informations ...</h5>
                <div class="row omb_row-sm-offset-3">
                    <div class="col-xs-12 col-sm-6">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control" name="name" placeholder="votre nom" value="{{ $currentUser->name }}">
                        </div>
                        <span class="help-block"></span>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="email" class="form-control" name="email" placeholder="adresse courriel" value="{{ $currentUser->email }}">
                        </div>

                        <span class="help-block"></span>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input  type="input" class="form-control datepicker" data-date-format="yyyy-mm-dd" id="dob" name="dob" placeholder="Date de naissance (yyyy-mm-dd)" value="{{$currentUser->dob}}">
                        </div>
                        <span class="help-block"></span>
                        
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="email" class="form-control" name="parent_email" placeholder="adresse courriel du parent" value="{{ $currentUser->parent_email }}">
                        </div>

                        <span class="help-block"></span>

                        <div class="form-group">
                            <select class="form-control" name="sex" id="sex">
                                <option {{$currentUser->sex == "" ? 'selected' : ''}} value="">Sexe</option>
                                <option {{$currentUser->sex == "male" ? 'selected' : ''}} value="male">Masculin</option>
                                <option {{$currentUser->sex == "female" ? 'selected' : ''}} value="female">Féminin</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <select class="form-control" name="ward" id="ward">
                                <option {{$currentUser->ward == "" ? 'selected' : ''  }} value="">Unité</option>
                                <option {{$currentUser->ward == "Alma" ? 'selected' : ''  }} value="Alma">Alma</option>
                                <option {{$currentUser->ward == "Chicoutimi" ? 'selected' : ''  }} value="Chicoutimi">Chicoutimi</option>
                                <option {{$currentUser->ward == "Drummondville" ? 'selected' : ''  }} value="Drummondville">Drummondville</option>
                                <option {{$currentUser->ward == "Granby" ? 'selected' : ''  }} value="Granby">Granby</option>
                                <option {{$currentUser->ward == "La " ? 'selected' : ''  }} value="La Prairie">La Prairie</option>
                                <option {{$currentUser->ward == "Lemoyne" ? 'selected' : ''  }} value="Lemoyne">Lemoyne</option>
                                <option {{$currentUser->ward == "Longueuil" ? 'selected' : ''  }} value="Longueuil">Longueuil</option>
                                <option {{$currentUser->ward == "Mont" ? 'selected' : ''  }} value="Mont-St-Hilaire">Mont-St-Hilaire</option>
                                <option {{$currentUser->ward == "Québec" ? 'selected' : ''  }} value="Québec">Québec</option>
                                <option {{$currentUser->ward == "Rimouski" ? 'selected' : ''  }} value="Rimouski">Rimouski</option>
                                <option {{$currentUser->ward == "Ste" ? 'selected' : ''  }} value="Ste-Foy">Ste-Foy</option>
                                <option {{$currentUser->ward == "Sherbrooke" ? 'selected' : ''  }} value="Sherbrooke">Sherbrooke</option>
                                <option {{$currentUser->ward == "St" ? 'selected' : ''  }} value="St-Jean">St-Jean</option>
                                <option {{$currentUser->ward == "Trois" ? 'selected' : ''  }} value="Trois-Rivières">Trois-Rivières</option>
                                <option {{$currentUser->ward == "Victoria" ? 'selected' : ''  }} value="Victoria">Victoria</option>
                                <option {{$currentUser->ward == "Victoriaville" ? 'selected' : ''  }} value="Victoriaville">Victoriaville</option>
                                <option {{$currentUser->ward == "Pieu " ? 'selected' : ''  }} value="Pieu de Montréal">Pieu de Montréal</option>
                                <option {{$currentUser->ward == "Mount " ? 'selected' : ''  }} value="Mount Royal Stake">Mount Royal Stake</option>
                                <option {{$currentUser->ward == "Autre" ? 'selected' : ''  }} value="Autre">Autre</option>
                            </select>
                        </div>
                        <span class="help-block"></span>
                        <div class="form-group">
                            <select class="form-control col-md-12" name="type" id="type">
                                <option {{$currentUser->type == '' ? 'selected' : ''}} value="">Type d'utilisateur</option>
                                <option {{$currentUser->type == 'youth' ? 'selected' : ''}} value="youth">Jeune</option>
                                <option {{$currentUser->type == 'parent' ? 'selected' : ''}} value="parent">Parent</option>
                                <option {{$currentUser->type == 'leader' ? 'selected' : ''}} value="leader">Dirigeant</option>
                            </select>
                        </div>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-info"></i></span>
                            <input type="text" class="form-control" name="allergies" placeholder="Allergies ou des préférences alimentaires ?" value="{{ $currentUser->allergies }}">
                        </div>

                        <span class="help-block">Si vous n'en avez pas, écrivez : Aucune</span>
                        <br/>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <br/>
                        
                        <button class="btn btn-lg btn-primary btn-block" id="submit" type="submit">Sauvegarder</button>

                        <span align="center" class="help-block error" id="error"></span>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
    <script src="../../../js/bootstrap.min.js"></script>
    <script src="../../../js/datepicker.min.js"></script>
    <script>
    $(document).ready(function() {
        $('.datepicker').datepicker({autoclose: true});

        $('#submit').click(function(e) {

            if ( $('#sex').val() == '' || $('#ward').val() == '' ||
                 $('#dob').val() == '' || $('#type').val() == '' ) {

                $('#error').text('Tous les champs sont obligatoires...');

                return false;
                e.preventDefault();
            }
        })

    });
    </script>
</html>