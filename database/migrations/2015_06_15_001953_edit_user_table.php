<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('avatar')->nullable();
            $table->string('parent_email')->nullable();
            $table->string('type')->nullable();
            $table->string('ward')->nullable();
            $table->string('dob')->nullable();
            $table->string('sex')->nullable();
            $table->integer('admin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('type');
            $table->dropColumn('ward');
            $table->dropColumn('parent_email');
            $table->dropColumn('dob');
            $table->dropColumn('sex');
        });
    }
}
