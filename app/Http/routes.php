<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'EventController@home');
Route::get('/home', 'EventController@home');
Route::get('/refresh', 'EventController@refresh');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/event/{eventId}/register', 'EventController@register');
    Route::get('/event/{eventId}/requirements', 'EventController@requirements');
    Route::post('/event/{eventId}/requirements', 'EventController@acceptRequirements');
    Route::get('/event/{eventId}/success', 'EventController@success');
    Route::get('profile/complete', 'UserController@completeProfile');
    Route::post('profile/update', 'UserController@storeCompletedProfile');

    Route::resource('/admin/inscriptions', 'InscriptionsController');
    Route::resource('/admin/events', 'EventController');
    Route::resource('/admin/users', 'UserController');
    Route::resource('/admin/venues', 'VenueController');
});

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('auth/callback', 'Auth\SocialController@handleProviderCallback');
Route::get('auth/social/{driver}', 'Auth\SocialController@redirectToProvider');
Route::get('event/{id}/remind', 'EventController@reminder');

