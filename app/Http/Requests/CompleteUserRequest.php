<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class CompleteUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'ward' => 'required',
            'dob' => 'required',
            'sex' => 'required',
            'type' => 'required',
            'allergies' => 'required',
            'parent_email' => 'required_if:type,youth|email|max:255',
        ];
    }
}
