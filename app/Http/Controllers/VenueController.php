<?php

namespace App\Http\Controllers;

use App\Http\Requests\VenueRequest;
use App\Repositories\VenueRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VenueController extends Controller
{

    /**
     * @var VenueRepositoryInterface
     */
    private $venueRepository;

    public function __construct(VenueRepositoryInterface $venueRepository)
    {

        $this->venueRepository = $venueRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $venues = $this->venueRepository->all();

        return view('admin/venues/index', compact('venues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin/venues/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VenueRequest $request
     * @return Response
     */
    public function store(VenueRequest $request)
    {
        $this->venueRepository->create($request->all());

        return redirect('/admin/venues');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $venue = $this->venueRepository->getById($id);

        return view('admin.venues.show', compact('venue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $venue = $this->venueRepository->getById($id);

        return view('admin.venues.edit', compact('venue'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param VenueRequest $request
     * @return Response
     */
    public function update($id, VenueRequest $request)
    {
        $this->venueRepository->update($id, $request->all());

        return redirect('/admin/venues');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return $this->venueRepository->destroy($id);
    }
}
