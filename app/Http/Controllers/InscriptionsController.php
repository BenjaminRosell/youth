<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditInscriptionRequest;
use App\Repositories\EventRepository;
use App\Repositories\InscriptionsRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class InscriptionsController extends Controller
{
    /**
     * @var InscriptionsRepositoryInterface
     */
    private $inscriptionRepository;

    public function __construct(InscriptionsRepositoryInterface $inscriptionRepository)
    {
        $this->inscriptionRepository = $inscriptionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $inscription = $this->inscriptionRepository->getById($id);

        return view('admin.inscriptions.edit', compact('inscription'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditInscriptionRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(EditInscriptionRequest $request, $id)
    {
        $inscription = $this->inscriptionRepository->update($request->only('option', 'wearable'), $id);

        return redirect('/admin/events/'. $inscription->event_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return $this->inscriptionRepository->destroy($id);
    }
}
