<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminUserRequest;
use App\Http\Requests\CompleteUserRequest;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = $this->userRepository->all();

        return view('admin/users/index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin/users/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CompleteUserRequest $request
     * @return Response
     */
    public function store(CompleteUserRequest $request)
    {
        $this->userRepository->create($request->all());

        return redirect('/admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->getById($id);

        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->getById($id);

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param AdminUserRequest $request
     * @return Response
     */
    public function update($id, AdminUserRequest $request)
    {
        $this->userRepository->update($request->all(), $id);

        return redirect('/admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return $this->userRepository->destroy($id);
    }


    public function completeProfile()
    {
        return view('user.complete');
    }

    public function storeCompletedProfile(CompleteUserRequest $userRequest)
    {
        $this->userRepository->update($userRequest, null);

        $eventId = \Cache::get(\Auth::user()->id . 'event');

        \Cache::forget(\Auth::user()->id . 'event');

        return redirect('/event/' . $eventId . ' /register');

    }
}
