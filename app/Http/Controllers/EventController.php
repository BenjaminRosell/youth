<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventRequest;
use App\Repositories\EventRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;

class EventController extends Controller
{

    /**
     * @var EventRepositoryInterfac
     */
    private $eventRepository;

    /**
     * @param EventRepositoryInterface $eventRepository
     */
    public function __construct(EventRepositoryInterface $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $events = $this->eventRepository->all();

        return view('admin/events/index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin/events/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CompleteUserRequest|EventRequest $request
     * @return Response
     */
    public function store(EventRequest $request)
    {
        $this->eventRepository->create($request->all());

        return redirect('/admin/events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $event = $this->eventRepository->getWithDataById($id);

        return view('admin.events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $event = $this->eventRepository->getById($id);

        return view('admin.events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param CompleteUserRequest $request
     * @return Response
     */
    public function update($id, EventRequest $request)
    {
        $this->eventRepository->update($request->all(), $id);

        return redirect('/admin/events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return $this->eventRepository->destroy($id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function home()
    {
        $events = $this->eventRepository->getUpcoming();

        return view('layout.event', compact('events'));
    }

    /**
     * Registers a given event to an event
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register($eventId, Request $request)
    {
        $user = \Auth::user();
        $event = $this->eventRepository->getById($eventId);

        if (!$user->isComplete()) {
            \Cache::add($user->id . 'event', $eventId, 10);

            return redirect('profile/complete');
        }

        if (($event->discharge or $event->rules or $event->health) and !$request->session()->pull('accepted')) {
            \Cache::add($user->id . 'event', $eventId, 10);

            return redirect('event/' . $eventId . '/requirements');
        }

        $this->eventRepository->register($eventId, $request->session()->pull('options'));

        $request->session()->forget('options');
        $request->session()->forget('accepted');

        return redirect('event/' . $eventId . '/success');
    }

    /**
     * Registers a given user to an event
     *
     * @return \Illuminate\View\View
     */
    public function success($eventId)
    {
        $event = $this->eventRepository->getbyId($eventId);

        return view('partials.success', compact('event'));
    }

    /**
     * Registers a given event to an event
     *
     * @return \Illuminate\View\View
     */
    public function requirements($eventId)
    {
        $event = $this->eventRepository->getbyId($eventId);

        return view('partials.requirements', compact('event'));
    }

    /**
     * Registers a given event to an event
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function acceptRequirements($eventId, Request $request)
    {
        $request->session()->put('accepted', true);
        $request->session()->put('options', [$request->input('option'), $request->input('wearable'), $request->input("notes")]);

        return redirect('event/' . $eventId . '/register');
    }

    /**
     * @return string
     */
    public function refresh()
    {
        return "<script language=javascript>parent.window.location.reload()</script>";
    }

    /**
     * @param $eventId
     */
    public function reminder($eventId) {
        
        $participants = $this->eventRepository->getById($eventId)->participants;

        foreach ($participants as $participant) {
            Mail::send('emails.reminder', ['user' => $participant], function($m) use ($participant) {
                $m->to($participant->email);
                if($participant->parent_email){
                    $m->cc($participant->parent_email);
                }
                $m->subject('La conférence jeunesse approche bientôt !');
            });
        }


    }
}
