<?php

namespace App\Http\Controllers\Auth;

use App\Repositories\UserRepositoryInterface;
use Illuminate\Routing\Controller;

class SocialController extends Controller
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {

        $this->userRepository = $userRepository;
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($driver)
    {
        return \Socialite::driver($driver)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = \Socialite::driver('facebook')->user();

        $this->userRepository->loginOrCreate($user);

        return redirect('/');

    }
}