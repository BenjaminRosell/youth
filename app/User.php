<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * @property mixed ward
 * @property string email
 * @property string name
 * @property string dob
 * @property string sex
 * @property string type
 * @property string parent_email
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'avatar', 'sex', 'type', 'dob', 'ward'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /**
     * @return bool
     */
    public function isComplete()
    {
        if ($this->ward and $this->dob and $this->sex and $this->type and $this->allergies )
            return true;

        return false;
    }

    /**
     * @return string
     */
    public function getGravatarAttribute()
    {
        return '//www.gravatar.com/avatar/' . md5($this->email) . '?size=50';
    }

    /**
     * @return string
     */
    public function getAvatarAttribute($value)
    {
        return str_replace('normal', 'square&width=200&height=200', $value);
    }

    /**
     * @return string
     */
    public function getBigGravatarAttribute()
    {
        return '//www.gravatar.com/avatar/' . md5($this->email) . '?size=270';
    }
}
