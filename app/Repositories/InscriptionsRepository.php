<?php

namespace App\Repositories;

use App\Inscription;
use App\Jobs\UserRegisteredToEvent;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;

class InscriptionsRepository implements InscriptionsRepositoryInterface
{
    use DispatchesJobs;
    /**
     * @var Event
     */
    private $model;

    /**
     * @param Event $event
     */
    public function __construct(Inscription $model)
    {
        $this->model = $model;
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * @param $data
     * @param mixed $id
     * @return mixed
     */
    public function update($data, $id ) {

        $inscription = $this->model->findOrFail($id);

        $data = is_array($data) ? json_decode(json_encode($data)) : $data;

        $inscription->wearable = $data->wearable;
        $inscription->option = $data->option;

        $inscription->save();

        return $inscription;
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }
}