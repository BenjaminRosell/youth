<?php

namespace App\Repositories;

use App\Venue;

class VenueRepository implements VenueRepositoryInterface
{

    /**
     * @var Venue
     */
    private $model;

    public function __construct(Venue $model)
    {

        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getByName($name)
    {
        return $this->model->where('name', 'LIKE', '%name%');
    }

    /**
     * @param $data
     * @return Venue
     */
    public function create($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function update($id, $data)
    {
        $model = $this->model->find($id);

        $model->fill($data)->save();

        return $model;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }
}
