<?php
/**
 * Created by PhpStorm.
 * User: benjamingonzalez
 * Date: 15-06-15
 * Time: 1:59 AM
 */

namespace App\Repositories;


interface UserRepositoryInterface {
    /**
     * @return mixed
     */
    public function all();

    /**
     * @param $data
     * @return mixed
     */
    public function update($data, $id);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $user
     * @return mixed
     */
    public function loginOrCreate($user);

    /**
     * @param $data
     * @return mixed
     */
    public function create($data);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);


}