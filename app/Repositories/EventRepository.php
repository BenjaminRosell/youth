<?php

namespace App\Repositories;

use App\Event;
use App\Jobs\UserRegisteredToEvent;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;

class EventRepository implements EventRepositoryInterface
{
    use DispatchesJobs;
    /**
     * @var Event
     */
    private $event;

    /**
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * @return mixed
     */
    public function getUpcoming()
    {
        return $this->event->where('starts_at', '>=', Carbon::now())->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->event->with('participants')->findOrFail($id);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getWithDataById($id)
    {
        return $this->event->with(['youth' => function ($query) {
                $query->where('type', 'youth');
            },'parents' => function ($query) {
                $query->where('type', 'parent');
            },'leaders' => function ($query) {
                $query->where('type', 'leader');
            }])
            ->findOrFail($id);
    }

    /**
     * @param $eventId
     * @param $options
     * @return bool
     */
    public function register($eventId, $options)
    {
        $event = $this->event->find($eventId);

        if ($event->userIsRegistered()) return false;

        $event->participants()->attach(\Auth::user()->id);

        $this->saveOption($options, $event->id);

        $this->dispatch(new UserRegisteredToEvent(\Auth::user(), $event, $this->userRegistration($eventId)));
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->event->all();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function update($data, $id)
    {
        return $this->event->update();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->event->destroy($id);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        if (isset($data['_token'])) {
            unset($data['_token']);
        }
        return $this->event->create($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getByHastag($hashtag)
    {
        return $this->event->where('hashtag', 'LIKE', "%$hashtag%");
    }

    /**
     * @param $option
     * @param $event
     */
    protected function saveOption($options, $eventId)
    {
        if ($options and $registration = $this->userRegistration($eventId)) {
            $pivot = $registration->pivot;
            $pivot->option = $options[0];
            $pivot->wearable = $options[1];
            if (isset($options[2])) {
                $pivot->notes = implode(', ', $options[2]);
            }
            $pivot->save();
        }
    }

    /**
     * @return mixed
     */
    public function userRegistration($eventId)
    {
        $userId = \Auth::user()->id;

        return $this->event->with(['participants' => function($q) use ($userId) {
            $q->where('user_id', $userId);
        }])->whereId($eventId)->first()->participants->first();
    }
}