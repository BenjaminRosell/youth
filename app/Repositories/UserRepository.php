<?php

namespace App\Repositories;

use App\User;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @var User
     */
    private $model;

    /**
     * @param User $model
     */
    public function __construct(User $model)
    {

        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param $id
     * @return integer
     */
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param $user
     * @return string
     */
    public function loginOrCreate($user) {

        if ($registredUser = $this->model->where('email', $user->email)->first()) {
            \Auth::login($registredUser);
            return 'login';
        }

        $createdUser = $this->create([
            'name' => $user->name,
            'avatar' => $user->avatar,
            'email' => $user->email,
            'sex' => $user->user['gender']
        ]);

        \Auth::login($createdUser);
        return 'create';
    }

    /**
     * @param $data
     * @return static
     */
    public function create($data) {

        $user = $this->model->create($data);

        $user = $this->checkAdminRole($user, $data);

        return $user;
    }

    /**
     * @param $data
     * @param mixed $id
     * @return mixed
     */
    public function update($data, $id = null) {

        if ($id) {
            $user = $this->model->findOrFail($id);
        } else {
            $user = \Auth::user();
        }

        $user = $this->checkAdminRole($user, $data);

        $data = is_array($data) ? json_decode(json_encode($data)) : $data;

        $user->email = $data->email;
        $user->dob = $data->dob;
        $user->name = $data->name;
        $user->sex = $data->sex;
        $user->ward = $data->ward;
        $user->type = $data->type;
        $user->allergies = @$data->allergies ?: NULL;
        $user->parent_email = @$data->parent_email ?: NULL;

        return $user->save();
    }

    /**
     * @param $user
     * @param $data
     * @return mixed
     */
    private function checkAdminRole($user, $data)
    {
        if (isset($data['admin']) and $data['admin'] == true) {
            $user->admin = 1;
        } else {
            $user->admin = 0;
        }
        $user->save();

        return $user;
    }
}
