<?php 

namespace App\Repositories;

interface EventRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getUpcoming();

    /**
     * @param $eventId
     * @return mixed
     */
    public function getById($eventId);
    /**
     * @return mixed
     */
    public function all();

    /**
     * @param $data
     * @return mixed
     */
    public function update($data, $id);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $data
     * @return mixed
     */
    public function create($data);

    /**
     * @param $id
     * @return mixed
     */
    public function getByHastag($hashtag);

    /**
     * @param $eventId
     * @param $options
     * @return mixed
     */
    public function register($eventId, $options);

    /**
     * @param $eventId
     * @return mixed
     */
    public function userRegistration($eventId);
}