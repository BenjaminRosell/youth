<?php

namespace App\Repositories;


interface VenueRepositoryInterface {
    /**
     * @param $id
     * @return mixed
     */
    public function all();

    /**
     * @param $name
     * @return mixed
     */
    public function getByName($name);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $data
     * @return mixed
     */
    public function create($data);

    /**
     * @param $data
     * @return mixed
     */
    public function update($id, $data);

    /**
     * @param $data
     * @return mixed
     */
    public function destroy($id);
}