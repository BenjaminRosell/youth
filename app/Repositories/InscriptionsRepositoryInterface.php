<?php


namespace App\Repositories;


interface InscriptionsRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $id
     * @return mixed
     */
    public function update($data, $id);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);
}