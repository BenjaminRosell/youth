<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property string rules
 * @property bool discharge
 * @property bool health
 */

class Event extends Model
{
    protected $dates = ['created_at', 'updated_at', 'starts_at', 'ends_at'];

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function venue()
    {
        return $this->belongsTo('App\Venue');
    }

    /**
     * @return $this
     */
    public function participants()
    {
        return $this->belongsToMany('App\User', 'events_users')->withTimestamps()->withPivot('id','option','wearable', 'notes');
    }

    /**
     * @return $this
     */
    public function youth()
    {
        return $this->belongsToMany('App\User', 'events_users')->withTimestamps()->withPivot('id','option','wearable', 'notes');
    }

    /**
     * @return $this
     */
    public function parents()
    {
        return $this->belongsToMany('App\User', 'events_users')->withTimestamps()->withPivot('id','option','wearable', 'notes');
    }

    /**
     * @return $this
     */
    public function leaders()
    {
        return $this->belongsToMany('App\User', 'events_users')->withTimestamps()->withPivot('id','option','wearable', 'notes');
    }

    public function userIsRegistered()
    {
        $userId = \Auth::user()->id;
        $eventId = $this->id;

        $isFound = $this->whereHas('participants', function($q) use ($userId, $eventId) {
            $q->where('user_id', $userId);
            $q->where('event_id', $eventId);
        })->first();

        return $isFound ? true : false;
    }
}
