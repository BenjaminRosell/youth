<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed fullAddress
 * @property mixed address
 * @property mixed city
 * @property mixed postal_code
 * @property mixed province
 * @method Venue findOrFail($id)
 * @method Venue where()
 * @method Venue find($id)
 */
class Venue extends Model
{
    protected $guarded = [];

    /**
     * @return string
     */
    public function getFullAddressAttribute()
    {
        return $this->address. ', '.$this->city . ', ' . $this->postal_code . ', ' . $this->province;
    }

    /**
     * @return string
     */
    public function getPartialAddressAttribute()
    {
        return $this->address. ', '.$this->city;
    }

    /**
     * @return string
     */
    public function getMapLinkAttribute()
    {
        $addressString = preg_replace('/[\s,]+/', '+', $this->fullAddress);
        return 'https://www.google.ca/maps/place/'.$addressString;
    }
}
