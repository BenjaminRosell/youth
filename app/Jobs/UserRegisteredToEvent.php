<?php

namespace App\Jobs;

use App\Event;
use App\Jobs\Job;
use App\User;
use Illuminate\Contracts\Bus\SelfHandling;

class UserRegisteredToEvent extends Job implements SelfHandling
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var Event
     */
    private $event;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Event $event
     * @param $registration
     */
    public function __construct(User $user, Event $event, $registration)
    {
        //
        $this->user = $user;
        $this->event = $event;
        $this->registration = $registration;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $event = $this->event;
        $registration = $this->registration;

        \Mail::send('emails.confirmation', compact('user', 'event', 'registration'), function($m) use ($user, $event) {
            $m->to($user->email, $user->name)
                ->subject('Vous êtes maintenant inscrit !')
                ->bcc('cbretonben@gmail.com');

            if ($user->parent_email) {
                $m->cc($user->parent_email);
            }

            if ($event->rules) {
                $path = public_path('files/Consentement.pdf');
                $m->attach($path);
            }

            if ($event->discharge) {
                $path = public_path('files/Camps-2016.pdf');
                $m->attach($path);
            }
        });
    }
}
