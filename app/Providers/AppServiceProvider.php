<?php

namespace App\Providers;

use App\Venue;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $view->with('isLoggedIn', \Auth::check());
            $view->with('currentUser', \Auth::user());
        });

        view()->composer('admin.events.form', function ($view) {
            $view->with('venues', Venue::all());
        });

        setlocale(LC_ALL, 'fr_CA.UTF-8');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\EventRepositoryInterface', 'App\Repositories\EventRepository');
        $this->app->bind('App\Repositories\UserRepositoryInterface', 'App\Repositories\UserRepository');
        $this->app->bind('App\Repositories\VenueRepositoryInterface', 'App\Repositories\VenueRepository');
        $this->app->bind('App\Repositories\InscriptionsRepositoryInterface', 'App\Repositories\InscriptionsRepository');
    }
}
