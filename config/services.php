<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'jeunesdj.com',
        'secret' => 'key-ce1e507d43d7fdbed4eb515e5c57c2e9',
    ],

    'mandrill' => [
        'secret' => env('MAIL_USERNAME'),
    ],

    'ses' => [
        'key' => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key' => '',
        'secret' => '',
    ],

    'facebook' => [
        'client_id' => '1588857238053146',
        'client_secret' => '4feb101f7e69e1cc8bc6f90705f000dd',
        'redirect' =>  'http://jeunesdj.com/auth/callback'
    ],

];
