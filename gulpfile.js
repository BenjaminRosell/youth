var elixir = require('laravel-elixir');
var BrowserSync = require('laravel-elixir-browsersync');

elixir(function(mix) {
    BrowserSync.init();
    mix.BrowserSync(
        {
            proxy           : "jgjf.app",
            logPrefix       : "Laravel Eixir BrowserSync",
            logConnections  : false,
            reloadOnRestart : false,
            notify          : false
        });
});